import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

//IMPORTE DE DASHBOARD ADMIN, CLIENTE, USUARIO
import { AdministratorPage } from '../administrator/administrator';
import { UserPage } from '../user/user';
//import { DashboardUserPage } from '../user/dashboard-user/dashboard-user';

//IMPORTAR SERVICIOS
import { AuthService } from '../../app/service/auth.service';
import { RegisterPage } from '../register/register';
import { ClientPage } from '../client/client';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  //PROPIEDADES
  private btnDisabled:boolean;
  private user:any = {
    username:"",
    password: ""
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public authentication: AuthService ) {
      this.btnDisabled = false;
  }

  public logIn() {
    if(this.user.username || this.user.password) {
      if(this.user.username) {
        if(this.user.password) {
          this.btnDisabled = true;
          this.authentication.authentication(this.user)
          .then(response => {
            console.log(response);
            if(response.state > 0) {
              let type:string = null;
              localStorage.setItem('currentUser', response.username);
              localStorage.setItem('currentEmail', response.email);
              localStorage.setItem('currentId', response.id);
              localStorage.setItem('currentState', response.estado);
              localStorage.setItem('currentRolId', response.type);
              switch(response.type) {
                case 1:{
                  type = 'user';
                  this.loading.create({
                    content: "Iniciando Sesión Usuario...",
                    duration: 500
                  }).present();
                  this.navCtrl.setRoot(UserPage);
                  break;
                }
                case 2:{
                  type = 'client';
                  this.loading.create({
                    content: "Iniciando Sesión...",
                    duration: 500
                  }).present();
                  this.navCtrl.setRoot(ClientPage);
                  break;
                }
                case 4:{
                  type = 'admin';
                  this.loading.create({
                    content: "Iniciando Sesión Administrador...",
                    duration: 500
                  }).present();
                  this.navCtrl.setRoot(AdministratorPage);
                  break;
                }
                default:{
                  type = 'usuario';
                  break;
                }
              }
              localStorage.setItem('currentType', type);
              console.clear();
            } else {
              this.btnDisabled = false;
              this.toast.create({
                message: "Su usuario se encuentra deshabilitado temporalmente.",
                duration: 3000
              }).present();
            }
          }).catch(error => {
            console.clear();
            if(error.status == 401){
              this.btnDisabled = false;
              this.toast.create({
                message: "Usuario o contraseña incorrectos.",
                duration: 3000
              }).present();
            }
          });
        } else {
          this.toast.create({
            message: "Ingrese una contraseña.",
            duration: 3000
          }).present();
        }
      } else {
        this.toast.create({
          message: "Ingrese un usuario.",
          duration: 3000
        }).present();
      }
    } else {
      this.toast.create({
        message: "Ingrese un usuario y contraseña.",
        duration: 3000
      }).present();
    }      
  }

  public createAccount() {
    this.navCtrl.push(RegisterPage);
  }
    
  
} 

