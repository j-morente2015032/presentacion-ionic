import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

//IMPORTE DE DASHBOARD ADMIN, CLIENTE, USUARIO
import { AdministratorPage } from '../administrator/administrator';
import { UserPage } from '../user/user';
import { ClientPage } from '../client/client';

//IMPORTAR SERVICIOS
import { AuthService } from '../../app/service/auth.service';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  //PROPIEDADES
  private idRol:any;

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public authentication: AuthService) {
    this.loading.create({
        content: "Cargando...",
        duration: 1000
    }).present();
    this.idRol = localStorage.getItem('currentRolId');
    if(this.idRol) {
      if(this.idRol == 1) {
        this.navCtrl.setRoot(UserPage);
      } else if(this.idRol == 2) {
        this.navCtrl.setRoot(ClientPage);
      } else if(this.idRol == 4) {
        this.navCtrl.setRoot(AdministratorPage);
      }
    } else {
      this.navCtrl.setRoot(LoginPage);
    }
  }
  
} 

