import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { ProfileAdminPage } from './profile-admin/profile-admin';
import { ProductsAdminPage } from './products-admin/products-admin';
import { EventsAdminPage } from './events-admin/events-admin';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'administrator',
  templateUrl: 'administrator.html'
})
export class AdministratorPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = ProductsAdminPage;
  pages: Array<{icon:string, title: string, component: any}>;

  constructor(public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public loading: LoadingController,
    private storage: Storage) {
    //this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { icon: 'ios-basket', title: 'Productos', component: ProductsAdminPage },
      { icon: 'ios-calendar', title: 'Eventos', component: EventsAdminPage },
      { icon: 'ios-person', title: 'Configuración de la Cuenta', component: ProfileAdminPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    let loader = this.loading.create({
      content: "Cargando ...",
      duration: 1000
      });
    loader.present();
    this.nav.setRoot(page.component);
  }

  logOut() {
    localStorage.clear();
    this.storage.clear();
    let loader = this.loading.create({
      content: "Cerrando Sesión...",
      duration: 1000
      });
    loader.present();
    this.nav.setRoot(LoginPage);
  }
}
