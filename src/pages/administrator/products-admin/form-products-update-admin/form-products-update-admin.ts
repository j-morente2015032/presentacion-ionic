import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProductsAdminPage } from './../products-admin';
import { ProductsService } from '../../../../app/service/products.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { path } from "../../../../app/config.module";

//JQUERY
declare var $:any;

@Component({
  selector: 'form-products-update-admin',
  templateUrl: 'form-products-update-admin.html'
})
export class ProductsFormUpdateAdminPage implements OnInit {
  //Propiedades
  private categories:any[] = [];
  private product = {
    name: '',
    description : '',
    price: '',
    quantity: '',
    cost : '',
    category: '',
    picture: '',
    id: ''
  }
  private title:any;
  private parameter:any;
  private basePath:string = path.path

  //Constructor
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public productsService: ProductsService,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController
  ) {
    this.title = "Actualizar Producto";
    this.loadAllCategories();
    this.parameter = this.navParams.get('parameter');
    this.productsService.getSingle(this.parameter)
    .then(response => {
      this.product.name = response.name;
      this.product.description = response.description;
      this.product.price = response.price;
      this.product.quantity = response.quantity;
      this.product.cost = response.cost;
      this.product.category = response.category;
      this.product.picture = response.picture;
      this.product.id = response.id;
    }).catch(error => {
      console.log(error)
    })
  }

  ngOnInit() {
  }

  //Cargar los productos
  public loadAllCategories(){
    this.categorysService.getAll()
    .then(response => {
      this.categories = response;
    }).catch(error => {
      console.clear
    })
  }

  //Subir Imagenes de Producto
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}product/upload/${this.product.id}`;
    
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        this.loading.create({
          content: "Cargando Imagen",
          duration: 2000
        }).present();

        $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta) {
            $('#imgAvatar').attr("src",'')
            $('#imgAvatar').attr("src",respuesta.picture)
            $("#"+id).val('')
          }
        );
      } else {
        this.toast.create({
          message: "La imagen es demasiado grande.",
          duration: 1500
        }).present();
      }
    } else {
      this.toast.create({
        message: "El tipo de imagen no es válido.",
        duration: 1500
      }).present();
    }
  }

  //Insertar Datos
  public update(){
    this.productsService.update(this.product)
    .then(response => {
      let loader = this.loading.create({
        content: "Actualizando Producto...",
        duration: 2000
      });
      loader.present();
      this.navCtrl.setRoot(ProductsAdminPage);
      console.clear
    }).catch(error => {
      console.clear
    })
  }

}