import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { ProductsService } from '../../../app/service/products.service';
import { ProductsFormAdminPage } from './form-products-admin/form-products-admin';
import { SeeProductsAdminPage } from './see-products-admin/see-products-admin';
import { ProductsFormUpdateAdminPage } from './form-products-update-admin/form-products-update-admin';

@Component({
  selector: 'products-admin',
  templateUrl: 'products-admin.html'
})
export class ProductsAdminPage {
  private products:any[] = [];
  userId:any;
  Table:any;
  constructor(
    public navCtrl: NavController,
    public productsService: ProductsService,
    public loading: LoadingController,
    public alertCtrl:AlertController
  ) {
    this.loadAll();
  }

  //Cargar los productos
  public loadAll(){
      this.productsService.getAll()
      .then(response => {
          this.products = response;
      }).catch(error => {
            console.clear
      })
  }

  //Ver Formulario Agregar
  public viewForm() {
    this.navCtrl.push(ProductsFormAdminPage);
  }

  //Ver Mas Detalles Del Producto
  public seeMore(parameter:any) {
    this.loading.create({
        content: "Cargando",
        duration: 500
    }).present();
    this.navCtrl.push(SeeProductsAdminPage, { parameter });
  }

  //Ver Formulario Actualizar
  public viewFormUpdate(parameter:any) {
    this.loading.create({
        content: "Cargando",
        duration: 500
    }).present();
    this.navCtrl.push(ProductsFormUpdateAdminPage, { parameter });
  }
 
  //Eliminar Productos
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas Eliminar El Producto?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.productsService.delete(id)
            .then(response => {
                this.loadAll()
                console.clear
                this.loading.create({
                    content: "Eliminando Producto",
                    duration: 2500
                }).present();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

}