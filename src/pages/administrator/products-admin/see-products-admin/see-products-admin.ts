import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProductsService } from '../../../../app/service/products.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';

@Component({
  selector: 'see-products-admin',
  templateUrl: 'see-products-admin.html'
})
export class SeeProductsAdminPage implements OnInit {
  private users:any[] = [];
  private categories:any[] = [];
  private product = {
    name: '',
	description : '',
	price: '',
	quantity: '',
	cost : '',
	user_created: '',
	category: '',
    created_at: '',
    update_at: '',
    picture: ''
  }
  private parameter:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public productsService: ProductsService,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController,
  ) {
      this.loadAllCategories();
      this.loadAllUsers();
      this.parameter = this.navParams.get('parameter');
      setTimeout(() => {
      this.loadProduct(this.parameter);
      }, 500);
  }

  ngOnInit() {
  }

  //Cargar los productos
  public loadAllCategories(){
      this.categorysService.getAll()
      .then(response => {
          this.categories = response;
      }).catch(error => {
            console.clear
      })
  }

  //Cargar los productos
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear();
    })
  }

  public loadProduct(idProduct:any) {
      this.productsService.getSingle(idProduct)
      .then(response => {
          this.product.name = response.name;
          this.product.description = response.description;
          this.product.price = response.price;
          this.product.quantity = response.quantity; 
          this.product.cost = response.cost;
          this.product.user_created = this.returnNameUser(response.user_created);
          this.product.category = this.returnNameCategory(response.category);
          this.product.created_at = response.created_at;
          this.product.update_at = response.updated_at;
          this.product.picture = response.picture;
      }).catch(error => {
          console.clear();
      })
  }

  //Devolver el Nombre de la Categoria
  public returnNameCategory(idCategory:any):any {
    for(var i = 0;i<this.categories.length;i++) {
      if(this.categories[i].id === idCategory) {
        return this.categories[i].name;
      }
    }
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

}