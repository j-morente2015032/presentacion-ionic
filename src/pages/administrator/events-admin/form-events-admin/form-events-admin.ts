import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { EventsAdminPage } from './../events-admin';
import { EventsService } from '../../../../app/service/events.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';

declare var google;
declare var marker;
@Component({
  selector: 'form-events-admin',
  templateUrl: 'form-events-admin.html'
})
export class EventsFormAdminPage implements OnInit {
  //Propiedades
  private positions:any
  private lat:any
  private lng:any
  private map: any;
  private users:any[] = [];
  private event = {
  address: '',
	description : '',
	date: '',
  time: '',
	latitude: '',
	longitude: '',
  user_created: '',
  place: '',
  place_id:'0'
  }
  private title:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public eventsService: EventsService,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController,
    public geolocation: Geolocation
  ) {
    this.title = "Nuevo Evento";
    this.loadAllUsers();
  }

  //Cargar los productos
  public loadAllUsers(){
      this.usersService.getAll()
      .then(response => {
          this.users = response;
      }).catch(error => {
            console.clear
      })
  }

  ngOnInit() {
  }

  ionViewDidLoad(){
    this.getPosition();
  }
     
  getPosition():any{
      this.geolocation.getCurrentPosition().then(response => {
      this.loadMap(response);
    })
    .catch(error =>{
      console.clear();
    })
  }

  public onMapClick(event) {
    this.positions = event.latLng;
    let positions1 = event.latLng + '';
    let pos = positions1.replace(')','').replace('(','').split(',')
    this.lat = pos[0]
    this.lng = pos[1]
    event.target.panTo(this.positions);
  }

  public loadMap(position: Geoposition){
  let latitude = position.coords.latitude;
  let longitude = position.coords.longitude;
  this.event.latitude = latitude.toString();
  this.event.longitude = longitude.toString();
    // create a new map by passing HTMLElement
    let mapEle: HTMLElement = document.getElementById('map');

    // create LatLng object
    let myLatLng = {lat: latitude, lng: longitude};

    // create map
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });
    marker.addListener('click', this.toggleBounce);
  }

  public toggleBounce() {
    if (marker.getAnimation() !== null) {
      marker.setAnimation(null);
    } else {
      marker.setAnimation(google.maps.Animation.BOUNCE);
    }
  }

  //Insertar Datos
  insert(){
    this.eventsService.create(this.event)
    .then(response => {
      let loader = this.loading.create({
      content: "Registrando Evento...",
      duration: 2000
      });
      loader.present();
      this.navCtrl.setRoot(EventsAdminPage);
      console.clear
    })
    .catch(error => {
        console.clear
    })
  }



}