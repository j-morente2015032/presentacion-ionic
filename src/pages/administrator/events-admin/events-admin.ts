import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { EventsService } from '../../../app/service/events.service';
import { SeeEventsAdminPage } from './see-events-admin/see-events-admin';
import { EventsFormAdminPage } from './form-events-admin/form-events-admin';
import { EventsFormUpdateAdminPage } from './form-events-update-admin/form-events-update-admin';

@Component({
  selector: 'events-admin',
  templateUrl: 'events-admin.html'
})
export class EventsAdminPage {
  //Propiedades
  private events:any[] = [];
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public eventsService: EventsService,
    public alertCtrl :AlertController
  ) {
    this.loadAll();
  }

  public loadAll() {
    this.eventsService.getAll().then(response => {
      this.events = response;
    }).catch(error => {
      console.clear();
    })
  }

  //Ver Más del Evento
  public viewMore(parameter: any) {
    this.loading.create({
      content: "Cargando",
      duration: 500
    }).present();
    this.navCtrl.push(SeeEventsAdminPage, { parameter });
  }

  //Agregar Evento
  public viewForm() {
    this.navCtrl.push(EventsFormAdminPage);
  }

  //Ver Formulario Actualizar
  public viewFormUpdate(parameter:any) {
    this.loading.create({
        content: "Cargando",
        duration: 500
    }).present();
    this.navCtrl.push(EventsFormUpdateAdminPage, { parameter });
  }

  //Eliminar Evento
  public delete(id:string){
    let confirm = this.alertCtrl.create({
    title: '¿Deseas eliminar el evento?',
    buttons: [
      {
        text: 'Cancelar',
        handler: () => {
        }
      },
      {
        text: 'Aceptar',
        handler: () => {
          this.eventsService.delete(id)
          .then(response => {
            this.loadAll()
              console.clear
              this.loading.create({
                content: "Eliminando Evento",
                duration: 2500
              }).present();
          }).catch(error => {
            console.clear
          })
        }
      }
    ]});
    confirm.present();
  }
}
