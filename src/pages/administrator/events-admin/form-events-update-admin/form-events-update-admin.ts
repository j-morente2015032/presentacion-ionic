import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { EventsAdminPage } from './../events-admin';
import { EventsService } from '../../../../app/service/events.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { path } from "../../../../app/config.module";

//JQUERY
declare var $:any;
declare var google;
declare var marker;

@Component({
  selector: 'form-events-update-admin',
  templateUrl: 'form-events-update-admin.html'
})
export class EventsFormUpdateAdminPage implements OnInit {
  //Propiedades
  private map: any;
  private event = {
    address: '',
    description : '',
    date: '',
    time: '',
    latitude: '',
    longitude: '',
    user_created: '',
    place: '',
    place_id:'0',
    picture: '',
    id: ''
  }
  private title:any;
  private parameter:any;
  private basePath:string = path.path

  //Constructor
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public eventsService: EventsService,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController
  ) {
    this.title = "Actualizar Evento";
    this.parameter = this.navParams.get('parameter');
    this.eventsService.getSingle(this.parameter)
    .then(response => {
      this.event.address = response.address;
      this.event.description = response.description;
      this.event.date = response.date;
      this.event.time = response.time;
      this.event.latitude = response.latitude;
      this.event.longitude = response.longitude;
      this.event.place = response.place;
      this.event.place_id = response.place_id;
      this.event.user_created = response.user_created;
      this.event.picture = response.picture;
      this.event.id = response.id;
      this.loadMap(this.event.latitude,this.event.longitude);
    }).catch(error => {
      console.clear();
    })
  }

  ngOnInit() {
  }

  public loadMap(lat:any, long:any){
  let latitude = lat;
  let longitude = long;
    
  let mapEle: HTMLElement = document.getElementById('map');

  // create LatLng object
  let myLatLng = {lat: latitude, lng: longitude};

  // create map
  this.map = new google.maps.Map(mapEle, {
    center: myLatLng,
    zoom: 17
  });

  marker = new google.maps.Marker({
    map: this.map,
    draggable: true,
    animation: google.maps.Animation.DROP,
    position: myLatLng
  });
  marker.addListener('click', this.toggleBounce);
}

public toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}

  //Subir Imagenes de Producto
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}event/upload/${this.event.id}`;
    
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        this.loading.create({
          content: "Cargando Imagen",
          duration: 2500
        }).present();

        $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta) {
            $('#imgAvatar').attr("src",'')
            $('#imgAvatar').attr("src",respuesta.picture)
            $("#"+id).val('')
          }
        );
      } else {
        this.toast.create({
          message: "La imagen es demasiado grande.",
          duration: 1500
        }).present();
      }
    } else {
      this.toast.create({
        message: "El tipo de imagen no es válido.",
        duration: 1500
      }).present();
    }
  }

  //Insertar Datos
  public update(){
    this.eventsService.update(this.event)
    .then(response => {
      this.loading.create({
        content: "Actualizando Evento...",
        duration: 2000
      }).present();
      this.navCtrl.setRoot(EventsAdminPage);
      console.clear
    }).catch(error => {
      console.clear
    })
  }

}