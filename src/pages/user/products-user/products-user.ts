import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ProductsService } from '../../../app/service/products.service';
import { OrdersUserPage } from '../orders-user/orders-user';
import { SeeProductsUserPage } from './see-products-user/see-products-user';
//import { OrdersUserPage } from '../orders-user/orders-user';

@Component({
  selector: 'products-user',
  templateUrl: 'products-user.html'
})
export class ProductsUserPage {
  //Propiedades
  private products:any[] = [];
  private parameter:any;

  //Constructor
  constructor(
    public navCtrl: NavController,
    public productsService: ProductsService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public navParams: NavParams
  ) {
    this.parameter = this.navParams.get('parameter');
    if(this.parameter) {
      setTimeout(() => {
        this.loadAllForCategories(this.parameter);
      }, 500);
    } else {
      this.loadAll();
    }
    
  }

  //Cargar los Productos
  public loadAllForCategories(id:any){
    this.productsService.getAll()
    .then(response => {
      for(let x of response) {
        if(x.category == id) {
          this.products.push(x);
        } 
      } 
    }).catch(error => {
      console.clear
    })
  }

  public loadAll() {
    this.productsService.getAll()
    .then(response => {
      this.products = response;
    }).catch(error => {
      console.clear
    })
  }

  public seeOrders(parameter:any) {
    this.loading.create({
      content: "Cargando",
      duration: 750
    }).present();
    this.navCtrl.push(OrdersUserPage, { parameter });
  }

  public seeMore(parameter:any) {
    this.loading.create({
      content: "Cargando",
      duration: 750
    }).present();
    this.navCtrl.push(SeeProductsUserPage, { parameter });
  }
  

}