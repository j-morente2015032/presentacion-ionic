import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProductsService } from '../../../app/service/products.service';
import { OrdersService } from '../../../app/service/orders.service';

@Component({
  selector: 'orders-user',
  templateUrl: 'orders-user.html'
})
export class OrdersUserPage implements OnInit {
  //Propiedades 
  private orders:any[] = [];
  private idProduct:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public productsService: ProductsService,
    public ordersService: OrdersService
  ) {
    this.idProduct = this.navParams.get('parameter');
    this.loadOrders(this.idProduct);
  }

  ngOnInit() {
  }

  public loadOrders(id:any) {
    this.ordersService.getOrdersProducts(id)
    .then(response => {
        this.orders = response;
    }).catch(error => {
        console.clear
    })
  }

}