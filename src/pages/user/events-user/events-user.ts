import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { EventsService } from '../../../app/service/events.service';
import { SeeEventsUserPage } from './see-events-user/see-events-user';

@Component({
  selector: 'events-user',
  templateUrl: 'events-user.html'
})
export class EventsUserPage {
  //Propiedades
  private events:any[] = [];
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public eventsService: EventsService,
    public alertCtrl :AlertController
  ) {
    this.loadAll();
  }

  public loadAll() {
    this.eventsService.getAll().then(response => {
      this.events = response;
    }).catch(error => {
      console.clear();
    })
  }

  //Ver Más del Evento
  public viewMore(parameter: any) {
    this.loading.create({
      content: "Cargando",
      duration: 500
    }).present();
    this.navCtrl.push(SeeEventsUserPage, { parameter });
  }

}
