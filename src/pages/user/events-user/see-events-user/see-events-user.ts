import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { EventsService } from '../../../../app/service/events.service';
import { UsersService } from '../../../../app/service/users.service';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;

@Component({
  selector: 'see-events-user',
  templateUrl: 'see-events-user.html'
})
export class SeeEventsUserPage implements OnInit {
  private users:any[] = [];  
  private comments:any[] = [];
  private map: any;
  private event = {
    description: '',
    longitude:0,
    latitude:0,
    assistants:0,
    interested:0,
    time:0,
    user: '',
    id: ''
  }
  private parameter:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public eventsService: EventsService,
    public usersService: UsersService,
    public loading: LoadingController,
    public geolocation: Geolocation
  ) {
      this.loadAllUsers();
      this.parameter = this.navParams.get('parameter');
      console.log(this.parameter);
      this.loadEvent(this.parameter);
      this.event.id = this.parameter;
      this.loadComments(this.parameter)
  }

  ngOnInit() {
  }

  public loadComments(id:any) {
    this.eventsService.getAllComments()
    .then(response => {
      for(let x of response) {
        if(x.event == id) {
          let comment = {
            comment: x.comment,
            user: this.returnNameUser(x.user)
          }
          this.comments.push(comment);
        } 
      } 
    }).catch(error => {
        console.clear
    })
  }

  //Cargar los eventos
  public loadAllUsers(){
      this.usersService.getAll()
      .then(response => {
          console.log(response);
          this.users = response;
      }).catch(error => {
            console.clear
      })
  }

  public loadMap(lat:any, long:any){
  let latitude = lat;
  let longitude = long;
  console.log(latitude, longitude);
    
    // create a new map by passing HTMLElement
    let mapEle: HTMLElement = document.getElementById('map');

    // create LatLng object
    let myLatLng = {lat: latitude, lng: longitude};

    // create map
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 18
    });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      new google.maps.Marker({
        position: myLatLng,
        map: this.map,
        title: 'Hello World!'
      });
      mapEle.classList.add('show-map');
    });
  }

  public loadEvent(idEvent:any) {
      this.eventsService.getSingle(idEvent)
      .then(response => {
          this.event.description = response.description;
          this.event.longitude = response.longitude;
          this.event.latitude = response.latitude; 
          this.event.assistants = response.assistants.length;
          this.event.interested = response.interested.length;
          this.event.user = this.returnNameUser(response.category);
          this.loadMap(this.event.latitude,this.event.longitude);

      }).catch(error => {
          console.clear
      })
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

}