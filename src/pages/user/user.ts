import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CategorysUserPage } from './categorys-user/categorys-user';
import { ProductsUserPage } from './products-user/products-user';
import { ProfileUserPage } from './profile-user/profile-user';
import { LoginPage } from '../login/login';
import { EventsUserPage } from './events-user/events-user';
import { MessagesUserPage } from './messages-user/messages-user';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'user',
  templateUrl: 'user.html'
})
export class UserPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = CategorysUserPage;
  pages: Array<{icon:string, title: string, component: any}>;

  constructor(public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public loading: LoadingController,
    private storage: Storage) {
    //this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { icon: 'md-apps', title: 'Categorias', component: CategorysUserPage },
      { icon: 'md-basket', title: 'Productos', component: ProductsUserPage },
      { icon: 'md-calendar', title: 'Eventos', component: EventsUserPage },
      { icon: 'md-send', title: 'Mensajería', component: MessagesUserPage },
      { icon: 'md-person', title: 'Configuración de la Cuenta', component: ProfileUserPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.loading.create({
      content: "Cargando ...",
      duration: 1000
      }).present();
    this.nav.setRoot(page.component);
  }

  logOut() {
    localStorage.clear();
    this.storage.clear();
    this.loading.create({
      content: "Cerrando Sesión...",
      duration: 1000
      }).present();
    this.nav.setRoot(LoginPage);
  }
}
