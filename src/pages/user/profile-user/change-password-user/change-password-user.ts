import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';
import { UsersService } from '../../../../app/service/users.service';
import { ProfileUserPage } from '../profile-user'

@Component({
  selector: 'change-password-user',
  templateUrl: 'change-password-user.html'
})
export class ChangePasswordProfileUserPage {
  //Propiedades
  private changePassword = {
    old_pass: '',
	new_pass : '',
	new_pass_rep: '',
    id: ''
  }
  private idUser:any;

  constructor(
    public navCtrl: NavController,
    public usersService: UsersService,
    public toast: ToastController,
    public loading: LoadingController
  ) {
      this.idUser
  }

  public updatePassword(){
    this.changePassword.id = localStorage.getItem('currentId');
    if(this.changePassword.old_pass == this.changePassword.new_pass) {
        this.toast.create({
            message: "No puede usar la misma contraseña.",
            duration: 1500
        }).present();
    } else if(this.changePassword.new_pass.length < 8) {
        this.toast.create({
            message: "La contraseña debe contener al menos 8 caracteres.",
            duration: 1500
        }).present();
    } else if(this.changePassword.new_pass.length > 8) {
        if(this.changePassword.new_pass == this.changePassword.new_pass_rep) {
              this.usersService.changePassword(this.changePassword)
              .then(response => {
                this.navCtrl.setRoot(ProfileUserPage)
                this.loading.create({
                    content: "Cambiando Contraseña",
                    duration: 500
                }).present();
              }).catch(error => {
                this.toast.create({
                    message: "Contraseña Inválida",
                    duration: 1500
                }).present();
              })
        } else {
            this.toast.create({
                message: "Las contraseñas no coinciden",
                duration: 1500
            }).present(); 
        }
    } else {
        this.toast.create({
            message: "Las contraseñas no coinciden",
            duration: 1500
        }).present(); 
    }

  }

}
