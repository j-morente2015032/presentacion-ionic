import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ChangePasswordProfileUserPage } from './change-password-user/change-password-user';
import { ProfileUpdateUserPage } from './profile-update-user/profile-update-user';
import { UsersTypesService } from '../../../app/service/users-types.service';

@Component({
  selector: 'profile-user',
  templateUrl: 'profile-user.html'
})
export class ProfileUserPage {
  //Propiedades
  private profilePicture:any;
  private idUser:any;
  
  constructor(
    public navCtrl: NavController,
    public usersTypesService: UsersTypesService
  ) {
    this.idUser = localStorage.getItem('currentId');
    this.usersTypesService.getSingle(this.idUser)
    .then(response => {
    this.profilePicture = response.picture;
    }).catch(error => {
        console.clear()
    })
  }

  //Cambiar Contraseña
  public changePassword() {
    this.navCtrl.push(ChangePasswordProfileUserPage);
  }

  //Actualizar Usuario
  public updateUser() {
    this.navCtrl.push(ProfileUpdateUserPage);
  }

}