import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { UsersService } from '../../../../app/service/users.service';
import { ProfileUserPage } from '../profile-user'
import { path } from "../../../../app/config.module";

//JQUERY
declare var $:any;

@Component({
  selector: 'profile-update-user',
  templateUrl: 'profile-update-user.html'
})
export class ProfileUpdateUserPage {
  //Propiedades
  private idUser:any;
  private basePath:string = path.path;
  private profile = {
    picture: ''
  }
  private profileUpdate = {
    username: '',
    email: '',
    firstname: '',
    lastname: '',
    work: '',
    description: '',
    age: '',
    birthday: '',
    phone: '',
    id: 0
  }
  
  //Constructor
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public usersService: UsersService,
    public loading: LoadingController
  ) {
    this.idUser = localStorage.getItem('currentId');
    this.usersService.getSingle(this.idUser)
    .then(response => {
        console.log(response)
        this.profile.picture = response.picture;
        this.profileUpdate.username = response.username;
        this.profileUpdate.email = response.email;
        this.profileUpdate.firstname = response.firstname;
        this.profileUpdate.lastname = response.lastname;
        this.profileUpdate.work = response.work;
        this.profileUpdate.description = response.description;
        this.profileUpdate.age = response.age;
        this.profileUpdate.birthday = response.birthday;
        this.profileUpdate.phone = response.phone;
        this.profileUpdate.id = this.idUser;
    }).catch(error => {
        console.log(error)
    })
  }

  //Subir Imagenes de Perfil
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/${this.idUser}`;
    
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        if(size<(2*(1024*1024))) {
            this.loading.create({
                content: "Cargando Imagen",
                duration: 3000
            }).present();
            $("#"+id).upload(url,
                {
                    avatar: archivos[0]
                },
                function(respuesta)
                {
                    $('#imgAvatar').attr("src",'')
                    $('#imgAvatar').attr("src",respuesta.picture)
                    $("#"+id).val('')
                }
            );
            
        } else {
                this.toast.create({
                    message: "La imagen es demasiado grande.",
                    duration: 1500
                }).present();
        }
    } else {
        this.toast.create({
            message: "El tipo de imagen no es válido.",
            duration: 1500
        }).present();
    }
  }

  //Insertar Datos
  public update(){
    this.usersService.update(this.profileUpdate)
    .then(response => {
      let loader = this.loading.create({
        content: "Actualizando Cuenta...",
        duration: 2000
      });
      loader.present();
      this.navCtrl.setRoot(ProfileUserPage);
      console.clear();
    }).catch(error => {
      console.clear();
    })
  }

}
