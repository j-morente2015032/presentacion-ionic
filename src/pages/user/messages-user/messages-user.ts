import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { MessagesService } from '../../../app/service/messages.service';
import { UsersService } from '../../../app/service/users.service';
import { MessagesClientUserPage } from './messages-client-user/messages-client-user';

@Component({
  selector: 'messages-user',
  templateUrl: 'messages-user.html'
})
export class MessagesUserPage {
  //Propiedades 
  private idUser:any;
  private messages:any[] = [];
  private users:any[] = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public messagesService: MessagesService,
    public usersService: UsersService
  ) {
    this.idUser = localStorage.getItem("currentId");
    this.loadAllUsers();
    setTimeout(() => {
      this.loadMessages(this.idUser);
    }, 1000);
  }

  public loadMessages(id:any) {
    let messagesTemp:any[] = [];
    this.messagesService.getAll()
    .then(response => {    
        for(let x of response) {
          if(x.user_receipt == id) {
            let name = this.returnNameUser(x.user_send);
            let user = {
              user: name,
              id: x.user_send
            }
            messagesTemp.push(user);
          }
        } 
        this.messages = this.removeDuplicates(messagesTemp, 'user')
        console
    }).catch(error => {
        console.clear
    })
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //Cargar los productos
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
        this.users = response;
    }).catch(error => {
          console.clear
    })
  }

  public messagesClientUser(parameter:any) {
    this.navCtrl.push(MessagesClientUserPage, { parameter });
  }

  public removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject  = {};

    for(var i in originalArray) {
       lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for(i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
     return newArray;
  }

}