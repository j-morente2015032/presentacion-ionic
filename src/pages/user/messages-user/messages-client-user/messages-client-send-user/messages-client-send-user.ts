import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { MessagesService } from '../../../../../app/service/messages.service';
import { MessagesUserClientPage } from '../../../../client/messages-client/messages-user-client/messages-user-client';

@Component({
  selector: 'messages-client-send-user',
  templateUrl: 'messages-client-send-user.html'
})
export class SendMessagesClientUserPage{
  private message = {
    subject: '',
    message : '',
    user_send: '',
    user_receipt: ''
  }
  private parameter:any;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public messagesService: MessagesService,
    public loading: LoadingController
  ) {
    this.parameter = this.navParams.get('parameter');
    this.message.user_receipt = this.parameter;
    this.message.user_send = localStorage.getItem('currentId');
  }

   //Insertar Datos
   public sendMessage(){
    let id = this.parameter;
    this.messagesService.create(this.message)
    .then(response => {
        let loader = this.loading.create({
        content: "Enviando Mensaje...",
        duration: 2000
        });
        loader.present();
        this.returnMessages(id)
        console.clear
        }).catch(error => {
            console.clear
    })
  }

  public returnMessages(parameter:any) {
    this.navCtrl.push(MessagesUserClientPage, { parameter });
  }

}