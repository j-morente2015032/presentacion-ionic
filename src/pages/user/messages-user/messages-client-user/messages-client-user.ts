import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { MessagesService } from '../../../../app/service/messages.service';
import { SendMessagesClientUserPage } from './messages-client-send-user/messages-client-send-user';

@Component({
  selector: 'messages-client-user',
  templateUrl: 'messages-client-user.html'
})
export class MessagesClientUserPage {
  //Propiedades 
  private messages:any[] = [];
  private parameter:any;
  private id:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public messagesService: MessagesService
  ) {
    this.parameter = this.navParams.get('parameter');
    this.id = this.parameter;
    this.loadMessages(this.parameter);
  }

  public loadMessages(user_receipt:any) {
    var idUser = localStorage.getItem("currentId");
    this.messagesService.getAll()
    .then(response => {      
        for(let x of response) {
          if((x.user_receipt == user_receipt && x.user_send == idUser) 
          || x.user_send == user_receipt && x.user_receipt == idUser) {
            this.messages.push(x);       
          }            
        }
    }).catch(error => {
        console.clear();
    })
  }

  public sendMessage(parameter:any) {
    this.navCtrl.push(SendMessagesClientUserPage, { parameter });
  }

}