import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

//IMPORTAR SERVICIOS
import { AuthService } from '../../app/service/auth.service';
import { UsersService } from '../../app/service/users.service';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  //PROPIEDADES
  private users:any[] = [];
  private btnDisabled:boolean;
  private user:any = {
    username: '',
    email: '',
    password: '',
    password_repeat: '',
    usuario: '',
    accept: false
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public authentication: AuthService,
    public usersService: UsersService) {
    this.loadAllUsers();
  }

  //Cargar los productos
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear();
    })
  }

  //Crear Cuenta
  public createAccount(){
    if(this.user.username || this.user.email || this.user.password 
    || this.user.password_repeat || this.user.usuario || this.user.accept) {
      if(this.user.username) {
        if(this.user.email) {
          if(this.user.password || this.user.password_repeat) {
            if(this.user.usuario) {
              if(this.user.accept == true) {
                if(this.user.password == this.user.password_repeat) {
                  this.btnDisabled = true;
                  this.usersService.create(this.user)
                  .then(response => {
                    this.loading.create({
                      content: "Registrando...",
                      duration: 2000
                    }).present();
                    this.navCtrl.setRoot(LoginPage);
                    console.clear();
                  }).catch(error => {
                    this.btnDisabled = false;
                    console.clear();
                    this.toast.create({
                        message: "El usuario ya existe.",
                        duration: 1500
                      }).present();
                  })
                } else {
                  this.toast.create({
                    message: "La contraseña deben ser iguales.",
                    duration: 1500
                  }).present();
                }
              } else {
                this.toast.create({
                    message: "No puedes crear tu usuario si no aceptas los términos y condiciones.",
                    duration: 1500
                }).present(); 
              }
            } else {
              this.toast.create({
                message: "Seleccione un proveedor.",
                duration: 1500
              }).present(); 
            }
          } else {
            this.toast.create({
              message: "La contraseña es requerida.",
              duration: 1500
            }).present(); 
          }
        } else {
          this.toast.create({
            message: "El correo es requerido.",
            duration: 1500
          }).present();  
        }
      } else {
        this.toast.create({
          message: "El nombre de usuario es requerido.",
          duration: 1500
        }).present();
      }
    } else {
      this.toast.create({
        message: "No dejar ningun campo vacío.",
        duration: 1500
      }).present();
    }    
  }
    
} 
