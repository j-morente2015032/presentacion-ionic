import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProductsService } from '../../../../app/service/products.service';
import { OrdersService } from '../../../../app/service/orders.service';

@Component({
  selector: 'my-orders',
  templateUrl: 'my-orders.html'
})
export class MyOrdersClientPage implements OnInit {
  //Propiedades 
  private idClient:any;
  private orders:any[] = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public productsService: ProductsService,
    public ordersService: OrdersService
  ) {
    this.idClient = localStorage.getItem("currentId");
    this.loadOrders(this.idClient);
  }

  ngOnInit() {
  }

  public loadOrders(id:any) {
    this.ordersService.getAll()
    .then(response => {
        for(let x of response) {
          if(x.user == id) {
            this.orders.push(x);
          } 
        } 
    }).catch(error => {
        console.clear
    })
  }

}