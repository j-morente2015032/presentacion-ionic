import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProductsService } from '../../../app/service/products.service';
import { OrdersService } from '../../../app/service/orders.service';

@Component({
  selector: 'orders-client',
  templateUrl: 'orders-client.html'
})
export class OrdersClientPage implements OnInit {
  //Propiedades 
  private order = {
    comment: '',
    quantity: '',
    unit_price: '',
    user: '',
    product: ''
  }
  private product:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public productsService: ProductsService,
    public ordersService: OrdersService
  ) {
    this.order.product = this.navParams.get('parameter');
    this.order.user = localStorage.getItem("currentId")
    this.loadProduct(this.order.product);
  }

  ngOnInit() {
  }

  public loadProduct(idProduct:any) {
    this.productsService.getSingle(idProduct)
    .then(response => {
        this.order.unit_price = response.price;
        this.product = response.name;
    }).catch(error => {
        console.clear
    })
  }

  //Insertar Datos
  insert(){
    this.ordersService.create(this.order)
    .then(response => {
        let loader = this.loading.create({
        content: "Realizando Pedido...",
        duration: 2000
        });
        loader.present();
        //this.navCtrl.setRoot(ProductsClientPage);
        console.clear
    }).catch(error => {
        console.clear
    })
  }

}