import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { CategorysService } from '../../../app/service/categorys.service';
import { ProductsClientPage } from '../products-client/products-client';

@Component({
  selector: 'categorys-client',
  templateUrl: 'categorys-client.html'
})
export class CategorysClientPage {
  //Propiedades
  private categorys:any[] = [];

  constructor(
    public navCtrl: NavController,
    public categorysService: CategorysService,
    public loading: LoadingController,
    public alertCtrl:AlertController
  ) {
    this.loadAll();
  }

  //Cargar los productos
  public loadAll(){
    this.categorysService.getAll()
    .then(response => {
      this.categorys = response;
    }).catch(error => {
      console.clear;
    })
  }

  //Ver Productos de la Categoria
  public seeProducts(parameter:any) {
    this.loading.create({
        content: "Cargando",
        duration: 750
    }).present();
    this.navCtrl.push(ProductsClientPage, { parameter });
  }

  
}