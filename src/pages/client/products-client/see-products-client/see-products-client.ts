import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProductsService } from '../../../../app/service/products.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { SeeProductsClientCommentPage } from './see-products-client-comment';

@Component({
  selector: 'see-products-client',
  templateUrl: 'see-products-client.html'
})
export class SeeProductsClientPage implements OnInit {
  private users:any[] = [];
  private comments:any[] = [];
  private product = {
    name: '',
    description : '',
    price: '',
    quantity: '',
    cost : '',
    user_created: '',
    category: '',
    created_at: '',
    update_at: '',
    picture: '',
    id: ''
  }
  private parameter:any;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public productsService: ProductsService,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController,
    public modalCtrl: ModalController
  ) {
    this.loadAllUsers();
    this.parameter = this.navParams.get('parameter');
    setTimeout(() => {
      this.loadProduct(this.parameter);
      this.loadComments(this.parameter);
    }, 500);
  }

  ngOnInit() {
  }

  //Cargar los productos
  public loadAllUsers(){
      this.usersService.getAll()
      .then(response => {
          this.users = response;
      }).catch(error => {
            console.clear;
      })
  }

  public loadProduct(idProduct:any) {
      this.productsService.getSingle(idProduct)
      .then(response => {
          this.product.name = response.name;
          this.product.description = response.description;
          this.product.price = response.price;
          this.product.quantity = response.quantity; 
          this.product.cost = response.cost;
          this.product.user_created = this.returnNameUser(response.user_created);
          this.product.created_at = response.created_at;
          this.product.update_at = response.updated_at;
          this.product.picture = response.picture;
          this.product.id = response.id;
      }).catch(error => {
          console.clear;
      })
  }

  public openPage(parameter:any) {
    this.navCtrl.push(SeeProductsClientCommentPage, { parameter });
  }

  public loadComments(idProduct:any) {
    this.productsService.getAllComment()
    .then(response => {
      for(let x of response) {
        if(x.product == idProduct) {
          let comment = {
            comment: x.comment,
            user: this.returnNameUser(x.user)
          }
          this.comments.push(comment);
        } 
      } 
    }).catch(error => {
        console.clear;
    })
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

}