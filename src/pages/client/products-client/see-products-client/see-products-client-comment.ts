import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ProductsService } from '../../../../app/service/products.service';
import { SeeProductsClientPage } from './see-products-client';

@Component({
  selector: 'see-products-client-comment',
  templateUrl: 'see-products-client-comment.html'
})
export class SeeProductsClientCommentPage{
  private comment = {
    comment: '',
    product : '',
    user: ''
  }
  private parameter:any;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public productsService: ProductsService,
    public loading: LoadingController
  ) {
    this.parameter = this.navParams.get('parameter');
    this.comment.product = this.parameter;
    this.comment.user = localStorage.getItem('currentId');
  }

   //Insertar Datos
   insert(){
    let id = this.comment.product;
    if(this.comment.comment) {
      this.productsService.createComment(this.comment)
      .then(response => {
        this.loading.create({
          content: "Registrando Comentario...",
          duration: 2000
        }).present();
        this.returnProducts(id)
        console.clear();
      }).catch(error => {
        console.clear();
      })
    } else {
      this.toast.create({
        message: "Ingrese un comentario.",
        duration: 3000
      }).present();
    }
  }

  public returnProducts(parameter:any) {
    this.navCtrl.push(SeeProductsClientPage, { parameter });
  }

}