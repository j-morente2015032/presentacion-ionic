import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ProductsService } from '../../../app/service/products.service';
import { OrdersClientPage } from '../orders-client/orders-client';
import { SeeProductsClientPage } from './see-products-client/see-products-client';

@Component({
  selector: 'products-client',
  templateUrl: 'products-client.html'
})
export class ProductsClientPage {
  //Propiedades
  private products:any[] = [];
  private parameter:any;

  //Constructor
  constructor(
    public navCtrl: NavController,
    public productsService: ProductsService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public navParams: NavParams
  ) {
    this.parameter = this.navParams.get('parameter');
    setTimeout(() => {
      this.loadAll(this.parameter);
    }, 500);
  }

  //Cargar los Productos
  public loadAll(id:any){
    this.productsService.getAll()
    .then(response => {
      for(let x of response) {
        if(x.category == id) {
          this.products.push(x);
        } 
      } 
    }).catch(error => {
      console.clear;
    })
  }

  public makeAnOrder(parameter:any) {
    this.loading.create({
      content: "Cargando",
      duration: 750
    }).present();
    this.navCtrl.push(OrdersClientPage, { parameter });
  }

  public seeMore(parameter:any) {
    this.loading.create({
      content: "Cargando",
      duration: 750
    }).present();
    this.navCtrl.push(SeeProductsClientPage, { parameter });
  }
  

}