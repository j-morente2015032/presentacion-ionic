import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { MessagesService } from '../../../../app/service/messages.service';
import { MessagesClientPage } from '../messages-client';
import { UsersService } from '../../../../app/service/users.service';

@Component({
  selector: 'send-messages-client',
  templateUrl: 'send-messages-client.html'
})
export class SendMessagesClientPage{
  private users:any[] = [];
  private message = {
    subject: '',
    message : '',
    user_send: '',
    user_receipt: ''
  }
  

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public messagesService: MessagesService,
    public usersService: UsersService,
    public loading: LoadingController
  ) {
    this.message.user_send = localStorage.getItem('currentId');
    this.loadAllUsers();
  }

   //Insertar Datos
   public sendMessage(){
    this.messagesService.create(this.message)
    .then(response => {
        this.loading.create({
        content: "Enviando Mensaje...",
        duration: 2000
        }).present();
        this.returnMessages()
        console.clear
        }).catch(error => {
            console.clear
    })
  }

  //Cargar los productos
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
          this.users = response;
    }).catch(error => {
            console.clear
    })
  }

  public returnMessages() {
    this.navCtrl.push(MessagesClientPage);
  }

}