import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { MessagesUserClientPage } from '../messages-user-client';
import { MessagesService } from '../../../../../app/service/messages.service';

@Component({
  selector: 'messages-user-send-client',
  templateUrl: 'messages-user-send-client.html'
})
export class SendMessagesUserClientPage{
  private message = {
    subject: '',
    message : '',
    user_send: '',
    user_receipt: ''
  }
  private parameter:any;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public messagesService: MessagesService,
    public loading: LoadingController
  ) {
    this.parameter = this.navParams.get('parameter');
    this.message.user_send = localStorage.getItem('currentId');
    this.message.user_receipt = this.parameter;
  }

   //Insertar Datos
   public sendMessage(){
    let id = this.parameter;
    this.messagesService.create(this.message)
    .then(response => {
        this.loading.create({
        content: "Enviando Mensaje...",
        duration: 2000
        }).present();
        this.returnMessages(id)
        console.clear
        }).catch(error => {
            console.clear
    })
  }

  public returnMessages(parameter:any) {
    this.navCtrl.push(MessagesUserClientPage, { parameter });
  }

}