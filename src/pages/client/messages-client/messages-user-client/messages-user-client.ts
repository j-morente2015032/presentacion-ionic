import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { MessagesService } from '../../../../app/service/messages.service';
import { SendMessagesUserClientPage } from './messages-user-send-client/messages-user-send-client';

@Component({
  selector: 'messages-user-client',
  templateUrl: 'messages-user-client.html'
})
export class MessagesUserClientPage {
  //Propiedades 
  private idClient:any;
  private messages:any[] = [];
  private parameter:any;
  private id:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public messagesService: MessagesService
  ) {
    this.parameter = this.navParams.get('parameter');
    this.idClient = localStorage.getItem("currentId");
    this.id = this.parameter;
    this.loadMessages(this.idClient, this.parameter);
  }

  public loadMessages(user_send:any, user_receipt:any) {
    this.messagesService.getAll()
    .then(response => {      
        for(let x of response) {
          if(x.user_send == user_send && x.user_receipt == user_receipt
            || (x.user_send == user_receipt && x.user_receipt == user_send)) {
            this.messages.push(x);       
          }            
        }
    }).catch(error => {
        console.clear
    })
  }

  public sendMessage(parameter:any) {
    this.navCtrl.push(SendMessagesUserClientPage, { parameter });
  }

}