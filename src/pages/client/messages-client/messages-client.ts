import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { MessagesService } from '../../../app/service/messages.service';
import { SendMessagesClientPage } from './send-messages-client/send-messages-client';
import { UsersService } from '../../../app/service/users.service';
import { MessagesUserClientPage } from './messages-user-client/messages-user-client';

@Component({
  selector: 'messages-client',
  templateUrl: 'messages-client.html'
})
export class MessagesClientPage {
  //Propiedades 
  private idClient:any;
  private messages:any[] = [];
  private users:any[] = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public messagesService: MessagesService,
    public usersService: UsersService
  ) {
    this.idClient = localStorage.getItem("currentId");
    this.loadAllUsers();
    setTimeout(() => {
      this.loadMessages(this.idClient);
    }, 750);
  }

  public loadMessages(id:any) {
    let messagesTemp:any[] = [];
    this.messagesService.getAll()
    .then(response => {     
        for(let x of response) {
          if(x.user_send == id) {
            let name = this.returnNameUser(x.user_receipt);
            let user = {
              user: name,
              id: x.user_receipt
            }
            messagesTemp.push(user);
          }
        } 
        this.messages = this.removeDuplicates(messagesTemp, 'user');
        console.clear;
    }).catch(error => {
        console.clear;
    })
  }

  public sendMessage() {
    this.navCtrl.setRoot(SendMessagesClientPage);
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //Cargar los productos
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  public messagesClientUser(parameter:any) {
    this.navCtrl.push(MessagesUserClientPage, { parameter });
  }

  public removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject  = {};

    for(var i in originalArray) {
       lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for(i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
     return newArray;
  }

}