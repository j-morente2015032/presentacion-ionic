import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CategorysClientPage } from './categorys-client/categorys-client';
import { LoginPage } from '../login/login';
import { MyOrdersClientPage } from './orders-client/my-orders/my-orders';
import { ProfileClientPage } from './profile-client/profile-client';
import { EventsClientPage } from './events-client/events-client';
import { MessagesClientPage } from './messages-client/messages-client';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'client',
  templateUrl: 'client.html'
})
export class ClientPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = CategorysClientPage;
  pages: Array<{icon:string, title: string, component: any}>;

  constructor(public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public loading: LoadingController,
    private storage: Storage) {
    //this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { icon: 'md-apps', title: 'Categorias', component: CategorysClientPage },
      { icon: 'md-cart', title: 'Pedidos', component: MyOrdersClientPage },
      { icon: 'md-calendar', title: 'Eventos', component: EventsClientPage },
      { icon: 'md-send', title: 'Mensajería', component: MessagesClientPage },
      { icon: 'md-person', title: 'Configuración de la Cuenta', component: ProfileClientPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    let loader = this.loading.create({
      content: "Cargando ...",
      duration: 1000
      });
    loader.present();
    this.nav.setRoot(page.component);
  }

  logOut() {
    localStorage.clear();
    this.storage.clear();
    let loader = this.loading.create({
      content: "Cerrando Sesión...",
      duration: 1000
      });
    loader.present();
    this.nav.setRoot(LoginPage);
  }
}
