import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { EventsService } from '../../../app/service/events.service';
import { SeeEventsClientPage } from './see-events-client/see-events-client';

@Component({
  selector: 'events-client',
  templateUrl: 'events-client.html'
})
export class EventsClientPage {
  //Propiedades
  private events:any[] = [];
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public eventsService: EventsService,
    public alertCtrl :AlertController
  ) {
    this.loadAll();
  }

  public loadAll() {
    this.eventsService.getAll().then(response => {
      this.events = response;
    }).catch(error => {
      console.clear();
    })
  }

  //Ver Más del Evento
  public viewMore(parameter: any) {
    this.loading.create({
      content: "Cargando",
      duration: 1000
    }).present();
    this.navCtrl.push(SeeEventsClientPage, { parameter });
  }

}
