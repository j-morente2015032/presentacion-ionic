import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { EventsService } from '../../../../app/service/events.service';
import { SeeEventsClientPage } from './see-events-client';

@Component({
  selector: 'see-events-client-comment',
  templateUrl: 'see-events-client-comment.html'
})
export class SeeEventsClientCommentPage{
  private comment = {
    comment: '',
    event : '',
    user: ''
  }
  private parameter:any;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public eventsService: EventsService,
    public loading: LoadingController
  ) {
    this.parameter = this.navParams.get('parameter');
    this.comment.event = this.parameter;
    this.comment.user = localStorage.getItem('currentId');
  }

   //Insertar Datos
  insert(){
    let id = this.comment.event;
    if(this.comment.comment) {
      this.eventsService.createComment(this.comment)
      .then(response => {
        this.loading.create({
          content: "Registrando Comentario...",
          duration: 2000
        }).present();
        this.returnEvents(id)
        console.clear();
      }).catch(error => {
        console.clear();
      })
    } else {
      this.toast.create({
        message: "Ingrese un comentario.",
        duration: 3000
      }).present();
    }
  }

  public returnEvents(parameter:any) {
    this.navCtrl.push(SeeEventsClientPage, { parameter });
  }

}