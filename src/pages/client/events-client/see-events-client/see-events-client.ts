import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { EventsService } from '../../../../app/service/events.service';
import { UsersService } from '../../../../app/service/users.service';
import { Geolocation } from '@ionic-native/geolocation';
import { SeeEventsClientCommentPage } from './see-events-client-comment';
import { UserEventsService } from '../../../../app/service/userevents.service';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

declare var google;

@Component({
  selector: 'see-events-client',
  templateUrl: 'see-events-client.html'
})
export class SeeEventsClientPage implements OnInit {
  private users:any[] = [];
  private comments:any[] = [];
  private map: any;
  private userEvents:any[] = [];
  private event = {
    description: '',
    longitude:0,
    latitude:0,
    assistants:0,
    interested:0,
    time:0,
    user: '',
    id: '',
    state: ''
  }
  private parameter:any;
  private changeAssistants:boolean = false;
  private changeInterested:boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public eventsService: EventsService,
    public usersService: UsersService,
    public loading: LoadingController,
    public geolocation: Geolocation,
    public usereventsService: UserEventsService,
    public alertCtrl: AlertController
  ) {
    this.loadAllUsers();
    this.loadAllEvent();
    this.parameter = this.navParams.get('parameter');
    setTimeout(() => {
      this.loadEvent(this.parameter);
      this.event.id = this.parameter;
      this.loadComments(this.parameter)
    }, 750);
    
  }

  ngOnInit() {
  }

  public loadComments(id:any) {
    this.eventsService.getAllComments()
    .then(response => {
      for(let x of response) {
        if(x.event == id) {
          let comment = {
            comment: x.comment,
            user: this.returnNameUser(x.user)
          }
          this.comments.push(comment);
        } 
      } 
    }).catch(error => {
        console.clear;
    })
  }

  //Cargar los Usuarios
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //Cargar los Eventos por Usuario
  public loadAllEvent(){
    this.usereventsService.getAll()
    .then(response => {
      this.userEvents = response;
    }).catch(error => {
      console.clear;
    })
  }

  public loadMap(lat:any, long:any){
  let latitude = lat;
  let longitude = long;
  
    let mapEle: HTMLElement = document.getElementById('map');

    let myLatLng = {lat: latitude, lng: longitude};

    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 18
    });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      new google.maps.Marker({
        position: myLatLng,
        map: this.map,
        title: 'Hello World!'
      });
      mapEle.classList.add('show-map');
    });
  }

  public loadEvent(idEvent:any) {
      this.eventsService.getSingle(idEvent)
      .then(response => {
        this.event.longitude = response.longitude;
        this.event.latitude = response.latitude;
        this.loadMap(this.event.latitude,this.event.longitude); 
        this.event.description = response.description;  
        this.event.assistants = response.assistants.length;
        this.event.interested = response.interested.length;
        this.event.user = this.returnNameUser(response.user.id);
        this.event.id = response.id;
        this.event.state = this.returnEventUser(response.id);
        if(this.event.state == '0') {
          this.changeAssistants = false;
          this.changeInterested = true;
        } else if (this.event.state == '1') {
          this.changeAssistants = true;
          this.changeInterested = false;
        } else {
          this.changeAssistants = false;
          this.changeInterested = false;
        }
      }).catch(error => {
          console.clear;
      })
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  public returnEventUser(idEvent:any):any {
    let idUser = localStorage.getItem("currentId");
    for(var i = 0;i<this.userEvents.length;i++) {
      if(this.userEvents[i].event == idEvent && this.userEvents[i].user == idUser) {
        return this.userEvents[i].state;
      }
    }
  }

  public openPage(parameter:any) {
    this.navCtrl.push(SeeEventsClientCommentPage, { parameter });
  }

  public interested() {
    let interested = {
      state: '0',
      user: localStorage.getItem("currentId"),
      event: this.parameter
    }
    let id = this.parameter;
    this.usereventsService.create(interested)
    .then(response => {
      this.loading.create({
        content: "Cargando...",
        duration: 1000
      }).present();
      this.loadAllEvent();
      this.changeAssistants = false;
      this.changeInterested = true;
      this.loadEvent(id);
      console.clear;
    }).catch(error => {
      console.clear;
    })
  }

  public assist() {
    let interested = {
      state: '1',
      user: localStorage.getItem("currentId"),
      event: this.parameter
    }
    let id = this.parameter;
    this.usereventsService.create(interested)
    .then(response => {
      this.loading.create({
        content: "Cargando...",
        duration: 1000
      }).present();
      this.loadAllEvent();
      this.loadEvent(id);
      this.changeAssistants = true;
      this.changeInterested = false;
      console.clear;
    }).catch(error => {
      console.clear;
    })
  }
}