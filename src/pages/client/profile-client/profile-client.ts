import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ChangePasswordProfileClientPage } from './change-password-client/change-password-client';
import { ProfileUpdateClientPage } from './profile-update-client/profile-update-client';
import { UsersService } from '../../../app/service/users.service';

@Component({
  selector: 'profile-client',
  templateUrl: 'profile-client.html'
})
export class ProfileClientPage {
  //Propiedades
  private profilePicture:any;
  private idClient:any;
  
  constructor(
    public navCtrl: NavController,
    public usersService: UsersService
  ) {
    this.idClient = localStorage.getItem('currentId');
    this.usersService.getSingle(this.idClient)
    .then(response => {
    this.profilePicture = response.picture;
    }).catch(error => {
    })
  }

  //Cambiar Contraseña
  public changePassword() {
    this.navCtrl.push(ChangePasswordProfileClientPage);
  }

  //Actualizar Usuario
  public updateClient() {
    this.navCtrl.push(ProfileUpdateClientPage);
  }

}