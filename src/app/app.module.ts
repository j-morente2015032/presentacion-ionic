import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//SERVICES
import { AuthService } from './service/auth.service';
import { CategorysService } from './service/categorys.service';
import { ProductsService } from './service/products.service';
import { UsersService } from './service/users.service';
import { UsersTypesService } from './service/users-types.service';
import { EventsService } from './service/events.service';
import { OrdersService } from './service/orders.service';
import { MessagesService } from './service/messages.service';

//PAGES ADMINISTRATOR
import { AdministratorPage } from '../pages/administrator/administrator';
import { ProductsAdminPage } from '../pages/administrator/products-admin/products-admin';
import { ProductsFormAdminPage } from '../pages/administrator/products-admin/form-products-admin/form-products-admin';
import { ProductsFormUpdateAdminPage } from '../pages/administrator/products-admin/form-products-update-admin/form-products-update-admin';
import { SeeProductsAdminPage } from '../pages/administrator/products-admin/see-products-admin/see-products-admin';
import { EventsAdminPage } from '../pages/administrator/events-admin/events-admin';
import { EventsFormAdminPage } from '../pages/administrator/events-admin/form-events-admin/form-events-admin';
import { EventsFormUpdateAdminPage } from '../pages/administrator/events-admin/form-events-update-admin/form-events-update-admin';
import { SeeEventsAdminPage } from '../pages/administrator/events-admin/see-events-admin/see-events-admin';
import { ProfileAdminPage } from '../pages/administrator/profile-admin/profile-admin';
import { ProfileUpdateAdminPage } from '../pages/administrator/profile-admin/profile-update-admin/profile-update-admin';
import { ChangePasswordProfileAdminPage } from '../pages/administrator/profile-admin/change-password-admin/change-password-admin';

//PAGES CLIENTE
import { RegisterPage } from '../pages/register/register';
import { ClientPage } from '../pages/client/client';
import { CategorysClientPage } from '../pages/client/categorys-client/categorys-client';
import { ProductsClientPage } from '../pages/client/products-client/products-client';
import { OrdersClientPage } from '../pages/client/orders-client/orders-client';
import { MyOrdersClientPage } from '../pages/client/orders-client/my-orders/my-orders';
import { ProfileClientPage } from '../pages/client/profile-client/profile-client';
import { ChangePasswordProfileClientPage } from '../pages/client/profile-client/change-password-client/change-password-client';
import { ProfileUpdateClientPage } from '../pages/client/profile-client/profile-update-client/profile-update-client';
import { MessagesClientPage } from '../pages/client/messages-client/messages-client';
import { SeeEventsClientPage } from '../pages/client/events-client/see-events-client/see-events-client';
import { SeeEventsClientCommentPage } from '../pages/client/events-client/see-events-client/see-events-client-comment';
import { SeeProductsClientPage } from '../pages/client/products-client/see-products-client/see-products-client';
import { SeeProductsClientCommentPage } from '../pages/client/products-client/see-products-client/see-products-client-comment';
import { EventsClientPage } from '../pages/client/events-client/events-client';

//PAGES USUARIO
import { UserPage } from '../pages/user/user';
import { CategorysUserPage } from '../pages/user/categorys-user/categorys-user';
import { ProductsUserPage } from '../pages/user/products-user/products-user';
import { ProfileUserPage } from '../pages/user/profile-user/profile-user';
import { ChangePasswordProfileUserPage } from '../pages/user/profile-user/change-password-user/change-password-user';
import { ProfileUpdateUserPage } from '../pages/user/profile-user/profile-update-user/profile-update-user';
import { OrdersUserPage } from '../pages/user/orders-user/orders-user';
import { EventsUserPage } from '../pages/user/events-user/events-user';

import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { SendMessagesClientPage } from '../pages/client/messages-client/send-messages-client/send-messages-client';
import { MessagesUserClientPage } from '../pages/client/messages-client/messages-user-client/messages-user-client';
import { SendMessagesUserClientPage } from '../pages/client/messages-client/messages-user-client/messages-user-send-client/messages-user-send-client';
import { MessagesUserPage } from '../pages/user/messages-user/messages-user';
import { MessagesClientUserPage } from '../pages/user/messages-user/messages-client-user/messages-client-user';
import { SendMessagesClientUserPage } from '../pages/user/messages-user/messages-client-user/messages-client-send-user/messages-client-send-user';
import { SeeEventsUserPage } from '../pages/user/events-user/see-events-user/see-events-user';
import { UserEventsService } from './service/userevents.service';
import { SeeProductsUserPage } from '../pages/user/products-user/see-products-user/see-products-user';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    //ADMINISTRADOR
    AdministratorPage,
    ProductsAdminPage,
      ProductsFormAdminPage,
      ProductsFormUpdateAdminPage,
      SeeProductsAdminPage,
    EventsAdminPage,
      EventsFormAdminPage,
      EventsFormUpdateAdminPage,
      SeeEventsAdminPage,
    ProfileAdminPage,
      ChangePasswordProfileAdminPage,
      ProfileUpdateAdminPage,
    //CLIENTE
    ClientPage,
    CategorysClientPage,
    ProductsClientPage,
      SeeProductsClientPage,
      SeeProductsClientCommentPage,
    EventsClientPage,
      SeeEventsClientPage,
      SeeEventsClientCommentPage,
    OrdersClientPage,
    MessagesClientPage,
      SendMessagesClientPage,
      MessagesUserClientPage,
      SendMessagesUserClientPage,
    MyOrdersClientPage,
    ProfileClientPage,
      ChangePasswordProfileClientPage,
      ProfileUpdateClientPage,
    //USUARIO
    UserPage,
    CategorysUserPage,
    ProductsUserPage,
      SeeProductsUserPage,
    OrdersUserPage,
    EventsUserPage,
      SeeEventsUserPage,
    MessagesUserPage,
      MessagesClientUserPage,
      SendMessagesClientUserPage,
    ProfileUserPage,
      ChangePasswordProfileUserPage,
      ProfileUpdateUserPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    //ADMINISTRADOR
    AdministratorPage,
    ProductsAdminPage,
      ProductsFormAdminPage,
      ProductsFormUpdateAdminPage,
      SeeProductsAdminPage,
    EventsAdminPage,
      EventsFormAdminPage,
      EventsFormUpdateAdminPage,
      SeeEventsAdminPage,
    ProfileAdminPage,
      ChangePasswordProfileAdminPage,
      ProfileUpdateAdminPage,
    //CLIENTE
    ClientPage,
    CategorysClientPage,
    ProductsClientPage,
      SeeProductsClientPage,
      SeeProductsClientCommentPage,
    EventsClientPage,
      SeeEventsClientPage,
      SeeEventsClientCommentPage,
    OrdersClientPage,
    MessagesClientPage,
      SendMessagesClientPage,
      MessagesUserClientPage,
      SendMessagesUserClientPage,
    MyOrdersClientPage,
    ProfileClientPage,
      ChangePasswordProfileClientPage,
      ProfileUpdateClientPage,
    //USUARIO
    UserPage,
    CategorysUserPage,
    ProductsUserPage,
      SeeProductsUserPage,
    OrdersUserPage,
    EventsUserPage,
      SeeEventsUserPage,
    MessagesUserPage,
      MessagesClientUserPage,
      SendMessagesClientUserPage,
    ProfileUserPage,
      ChangePasswordProfileUserPage,
      ProfileUpdateUserPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    //SERVICIOS
    AuthService,
    CategorysService,
    ProductsService,
    UsersTypesService,
    UsersService,
    EventsService,
    UserEventsService,
    OrdersService,
    MessagesService,
    GoogleMaps,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
