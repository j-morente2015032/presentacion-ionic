import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

import { path } from "../config.module";

import "rxjs/add/operator/toPromise";

@Injectable()
export class AuthService {
  headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  private basePath:string = path.path;

  constructor(private http:Http) {
  }

  private handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //Autenticación
  public authentication(login:any):Promise<any> {
    let url = `${this.basePath}login`
    return this.http.post(url,login)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Recovery
  public recovery(form:any):Promise<any>{
    let url = `${this.basePath}usuarios/password/reset`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  
  public logOut():Promise<any> {
    let url = `${this.basePath}logout`;
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

}